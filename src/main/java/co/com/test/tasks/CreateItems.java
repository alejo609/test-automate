package co.com.test.tasks;

import co.com.test.interactions.Wait;
import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.test.userinterfaces.CreateItemsPage.*;
import static co.com.test.userinterfaces.CreateProcessPage.*;
import static co.com.test.userinterfaces.IndexPage.*;
import static co.com.test.utils.Constants.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class CreateItems implements Task {

    public <T extends Actor> void performAs(T theActor) {
        theActor.attemptsTo(
                WaitUntil.the((BUTTON_MEETING),isVisible()),
                Wait.theSeconds(5),
                Click.on(BUTTON_MEETING),
                Wait.theSeconds(5),
                WaitUntil.the((BUTTON_MEETINGS),isVisible()),
                Wait.theSeconds(5),
                Click.on(BUTTON_MEETINGS),
                Wait.theSeconds(5),
                Click.on(NEW_MEETINGS),
                Wait.theSeconds(5),
                Click.on( CREATE_ITEM_MEETING ),
                Clear.field(INSERT_NAME_ITEM_MEETING),
                WaitUntil.the((INSERT_NAME_ITEM_MEETING),isVisible()),
                SendKeys.of(NAME_MEETING).into(INSERT_NAME_ITEM_MEETING),
                Click.on(BUTTON_INSERT_NEW_ITEM ),
                Click.on(LOCATION_MEETING_BUTTON),
                WaitUntil.the((TEXT_LOCATION_MEETING_BUTTON),isVisible()),
                SendKeys.of(CITY).into(TEXT_LOCATION_MEETING_BUTTON),
                Click.on(BUTTON_LOCATION_MEETING),
                Click.on(UNIT_MEETING_BUTTON),
                WaitUntil.the((TEXT_SPACE_UNIT_MEETING_BUTTON),isVisible()),
                SendKeys.of(UNIT).into(TEXT_SPACE_UNIT_MEETING_BUTTON),
                Click.on(BUTTON_TEXT_SPACE_UNIT_MEETING_BUTTON),
                WaitUntil.the((ORGANIZEDBY_BUTTON),isVisible()),
                Click.on(ORGANIZEDBY_BUTTON),
                WaitUntil.the((ORGANIZEDBY_FIRSTNAME ),isVisible()),
                Click.on(ORGANIZEDBY_FIRSTNAME),
                SendKeys.of(FIRST_NAME).into( ORGANIZEDBY_FIRSTNAME ),
                WaitUntil.the((ORGANIZEDBY_LASTNAME ),isVisible()),
                SendKeys.of(LAST_NAME).into(ORGANIZEDBY_LASTNAME),
                WaitUntil.the((ORGANIZEDBY_EMAIL ),isVisible()),
                SendKeys.of(EMAIL).into(ORGANIZEDBY_EMAIL),
                WaitUntil.the((BUTTON_ORGANIZEDBY),isVisible()),
                Wait.theSeconds(5),
                Click.on(BUTTON_ORGANIZEDBY)
                );
    }
    public static CreateItems createItems(){
        return instrumented(CreateItems.class);
    }
}

package co.com.test.tasks;

import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.test.userinterfaces.CreateItemsPage.*;
import static co.com.test.userinterfaces.CreateItemsPage.BUTTON_ORGANIZEDBY;
import static co.com.test.userinterfaces.IndexPage.*;
import static co.com.test.utils.Constants.NAME_UNIT_TEXT;
import static co.com.test.utils.Constants.PARENT_UNIT_TEXT;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectOrganization implements Task {

    public <T extends Actor> void performAs(T theActor){
        theActor.attemptsTo(
                WaitUntil.the((BUTTON_ORGANIZATION),isVisible()),
                Click.on(BUTTON_ORGANIZATION),
                WaitUntil.the((BUTTON_BUSINESS_UNITS),isVisible()),
                Click.on(BUTTON_BUSINESS_UNITS),
                WaitUntil.the((BUTTON_NEW_BUSINESS_UNITS),isVisible()),
                Click.on(BUTTON_NEW_BUSINESS_UNITS),
                WaitUntil.the((NAME_UNIT),isVisible()),
                SendKeys.of(NAME_UNIT_TEXT).into(NAME_UNIT),
                WaitUntil.the((PARENT_UNIT),isVisible()),
                Click.on(PARENT_UNIT),
                WaitUntil.the((SELECT_UNIT),isVisible()),
                SendKeys.of(PARENT_UNIT_TEXT).into(SELECT_UNIT),
                Click.on(SELECTED_UNIT),
                WaitUntil.the((BUTTON_SELECTED_UNIT),isVisible()),
                Click.on(BUTTON_SELECTED_UNIT)
        );
    }

    public static  SelectOrganization selectOrganization (){
        return instrumented(SelectOrganization.class);
    }
}
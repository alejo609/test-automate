package co.com.test.tasks;

import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import java.security.Key;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Login implements Task {
    public <T extends Actor> void performAs(T theActor){
        theActor.attemptsTo(
                WaitUntil.the((LoginPage.EMAIL),isVisible()),
                Clear.field(LoginPage.EMAIL),
                SendKeys.of(Constants.NICK).into(LoginPage.EMAIL),
                WaitUntil.the((LoginPage.PASS),isVisible()),
                Clear.field(LoginPage.PASS),
                SendKeys.of(Constants.PASS).into(LoginPage.PASS),
                Click.on(LoginPage.BUTTON_LOGIN)
        );

    }

    public static Login login(){
        return instrumented(Login.class);
    }
}
package co.com.test.tasks;

import co.com.test.interactions.Wait;
import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.test.userinterfaces.CreateProcessPage.*;
import static co.com.test.utils.Constants.UNIT;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class CreateUnit implements Task {
    public <T extends Actor> void performAs(T theActor) {
        theActor.attemptsTo(
                WaitUntil.the((UNIT_BUTTON_ORGANIZATION), isVisible()),
                Wait.theSeconds(8),
                Click.on(UNIT_BUTTON_ORGANIZATION),
                Wait.theSeconds(8),
                WaitUntil.the((BUTTON_BUSINESS_UNIT_ORGANIZATION ), isVisible()),
                Click.on(BUTTON_BUSINESS_UNIT_ORGANIZATION ),
                Wait.theSeconds(8),
                Click.on(BUTTON_NEW_BUSINESS_UNIT_ORGANIZATION ),
                SendKeys.of(UNIT).into(INSERT_NAME_NEW_BUSINESS_UNIT_ORGANIZATION),
                Wait.theSeconds(8),
                Click.on(SAVED_NAME_NEW_BUSINESS_UNIT_ORGANIZATION)
        );
    }
    public static CreateUnit createUnit(){
        return instrumented(CreateUnit.class);
    }
}
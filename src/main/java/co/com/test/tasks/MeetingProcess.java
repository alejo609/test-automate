package co.com.test.tasks;

import co.com.test.interactions.Wait;
import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;
import org.openqa.selenium.Keys;

import static co.com.test.userinterfaces.IndexPage.*;
import static co.com.test.utils.Constants.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class MeetingProcess implements Task {


    public <T extends Actor> void performAs(T theActor){
        theActor.attemptsTo(
                WaitUntil.the((BUTTON_MEETINGS),isVisible()),
                Wait.theSeconds(3),
                Click.on(BUTTON_MEETINGS),
                Click.on(NEW_MEETINGS),
                SendKeys.of(Constants.ACTOR).into(MEETING_NAME),
                Click.on(SELECT_LOCATION_MEETING),
                WaitUntil.the((INTO_SELECT_LOCATION_MEETING),isVisible()),
                Click.on(INTO_SELECT_LOCATION_MEETING),
                SendKeys.of(CITY).into(INTO_SELECT_LOCATION_MEETING),
                Click.on(LOCATION_MEETING_SELECTED),
                Click.on(UNIT_LOCATION_MEETING),
                SendKeys.of(UNIT).into(INFORMATION_UNIT_LOCATION_MEETING),
                Click.on(UNIT_LOCATION_MEETING_SELECTED),
                Click.on(ORGANIZEDBY_MEETING),
                SendKeys.of(ORGANIZER).into(INFORMATION_ORGANIZEDBY_MEETING),
                Click.on(ORGANIZER_MEETING),
                WaitUntil.the((SELECTED_REPORTER_MEETING_OPTION),isVisible()),
                Click.on(SELECTED_REPORTER_MEETING_OPTION),
                WaitUntil.the((INFORMATION_SELECTED_REPORTER_MEETING_OPTION),isVisible()),
                SendKeys.of(ORGANIZER).into(INFORMATION_SELECTED_REPORTER_MEETING_OPTION),
                WaitUntil.the((BUTTON_INFORMATION_SELECTED_REPORTER_MEETING_OPTION),isVisible()),
                Click.on(BUTTON_INFORMATION_SELECTED_REPORTER_MEETING_OPTION),
                WaitUntil.the((ATTENDEE_LIST_SELECT),isVisible()),
                Click.on(ATTENDEE_LIST_SELECT),
                WaitUntil.the((ATTENDEE_LIST_INPUT_INFORMATION),isVisible()),
                SendKeys.of(ORGANIZER).into(ATTENDEE_LIST_INPUT_INFORMATION),
                WaitUntil.the((BUTTON_ATTENDEE_LIST_INPUT_INFORMATION),isVisible()),
                Click.on(BUTTON_ATTENDEE_LIST_INPUT_INFORMATION),
                WaitUntil.the((BUTTON_SAVE_MEETING_PROCESS),isVisible()),
                Click.on(BUTTON_SAVE_MEETING_PROCESS)
                );
    }

    public static  MeetingProcess meetingProcess (){
        return instrumented(MeetingProcess.class);
    }
}
package co.com.test.tasks;

import co.com.test.interactions.Wait;
import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Clear;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.test.userinterfaces.IndexPage.BUTTON_MEETING;
import static co.com.test.userinterfaces.IndexPage.BUTTON_MEETINGS;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class SelectMeeting implements Task {
    public <T extends Actor> void performAs(T theActor){
        theActor.attemptsTo(
                Wait.theSeconds(5),
                WaitUntil.the((BUTTON_MEETING),isVisible()),
                Click.on(BUTTON_MEETING),
                Wait.theSeconds(5),
                WaitUntil.the((BUTTON_MEETINGS),isVisible()),
                Wait.theSeconds(5),
                Click.on(BUTTON_MEETINGS)
        );
    }

    public static SelectMeeting selectMeeting(){
        return instrumented(SelectMeeting.class);
    }

}
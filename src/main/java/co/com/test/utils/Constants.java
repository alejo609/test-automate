package co.com.test.utils;

public class Constants {
    public  static  final String LINK_WEB= "https://serenity.is/demo/";
    public  static  final String CITY= "Bello";
    public  static final  String UNIT="UNIDAD1";
    public static final String NICK ="admin";
    public static final String EMAIL ="alejandro@gmail.com";
    public static final String FIRST_NAME ="Luis Alejandro";
    public static final String LAST_NAME ="Aguirre Sossa";

    public static final String PASS ="serenity";
    public static final String ACTOR ="Alejandro";
    public static final String NAME_MEETING ="Reunion Serenity-Cypress";
    public static final String ORGANIZER= "Luis Alejandro Aguirre Sossa";

    public  static  final String NAME_UNIT_TEXT= "Unidad1";
    public  static  final String PARENT_UNIT_TEXT= "UNIDAD1";

}

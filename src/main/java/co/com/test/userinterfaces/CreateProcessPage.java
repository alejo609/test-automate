package co.com.test.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CreateProcessPage {
    public  static  final Target UNIT_BUTTON_ORGANIZATION = Target.the("Buton organizacion").located(By.xpath("//span[.='Organization']"));
    public  static  final Target BUTTON_BUSINESS_UNIT_ORGANIZATION = Target.the("Buton organizacion").located(By.xpath("//span[.='Business Units']"));
    public  static  final Target BUTTON_NEW_BUSINESS_UNIT_ORGANIZATION = Target.the("Buton organizacion").located(By.xpath("//div[@class='tool-button add-button icon-tool-button']"));
    public static final Target INSERT_NAME_NEW_BUSINESS_UNIT_ORGANIZATION = Target.the("Input donde se ingresa la unidad").located(By.xpath("//input[@name='Name']"));
    public static final Target SAVED_NAME_NEW_BUSINESS_UNIT_ORGANIZATION = Target.the("Boton save de la unidad").located(By.xpath("//div[@class='tool-button save-and-close-button icon-tool-button']"));
}
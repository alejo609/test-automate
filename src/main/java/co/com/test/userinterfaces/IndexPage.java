package co.com.test.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class IndexPage {
    public  static  final Target BUTTON_MEETING = Target.the("Buton reunion Meeting").located(By.xpath("//span[.='Meeting']"));
    public  static  final Target BUTTON_MEETINGS = Target.the("Buton reunion Meetings").locatedBy(("//span[.='Meetings']"));
    public static final Target BUTTON_ORGANIZATION   = Target.the("Buton organización").locatedBy(("//span[.='Organization']"));
    public static final Target BUTTON_BUSINESS_UNITS   = Target.the("Unidades de negocio").located(By.xpath("//span[.='Business Units']"));
    public static final Target BUTTON_NEW_BUSINESS_UNITS   = Target.the("Nuevas unidades de negocio").locatedBy("(//span[@class='button-inner'])");
    public static final Target NAME_UNIT  = Target.the("Espacio de nombre de unidad").located(By.xpath("//input[@name='Name']"));
    public static final Target PARENT_UNIT  = Target.the("Unidad").located(By.xpath("//span[@class='select2-chosen']"));
    public static final Target SELECT_UNIT = Target.the("Input donde se ingresa la contraseña").located(By.id("s2id_autogen1_search"));
    public  static  final  Target SELECTED_UNIT = Target.the("Unidad seleccionada").located(By.xpath("//span[@class='select2-match']"));
    public  static  final Target BUTTON_SELECTED_UNIT = Target.the("Button seleccionar unidad").located(By.xpath("//div[@class='tool-button save-and-close-button icon-tool-button']"));
    public  static  final Target NEW_MEETINGS = Target.the("Buton reunion").located(By.xpath("//div[@class='tool-button add-button icon-tool-button'] "));
    public static final Target MEETING_NAME = Target.the("Nombre de la reunion").located(By.xpath("//input[@name='MeetingName']"));
    public  static  final Target SELECT_LOCATION_MEETING = Target.the("boton 'select' lugar de la reunion").locatedBy("//span[@id='select2-chosen-7']");
    public  static  final Target INTO_SELECT_LOCATION_MEETING = Target.the("lugar de la reunion").located(By.xpath("//input[@id='s2id_autogen7_search']"));
    public  static  final Target LOCATION_MEETING_SELECTED = Target.the("lugar de la reunion").located(By.xpath("//div[@class='select2-result-label']"));
    public  static  final Target UNIT_LOCATION_MEETING = Target.the("Seleccionar la unidad de la reunion").located(By.xpath("//span[@id='select2-chosen-8']"));
    public  static  final Target INFORMATION_UNIT_LOCATION_MEETING = Target.the("ingreso Unidad de la reunion").located(By.xpath("//input[@id='s2id_autogen8_search']"));
    public  static  final Target UNIT_LOCATION_MEETING_SELECTED = Target.the("lugar de la reunion").located(By.xpath("//div[@class='select2-result-label']"));
    public  static  final Target ORGANIZEDBY_MEETING = Target.the("Seleccionar la unidad de la reunion").located(By.xpath("//span[@id='select2-chosen-9']"));
    public  static  final Target INFORMATION_ORGANIZEDBY_MEETING = Target.the("ingreso Unidad de la reunion").located(By.xpath("//input[@id='s2id_autogen9_search']"));
    public  static  final Target ORGANIZER_MEETING = Target.the("Seleccionar el reporter").located(By.xpath("//span[.='Luis Alejandro Aguirre Sossa']"));
    public  static  final Target SELECTED_REPORTER_MEETING_OPTION = Target.the("Click en opcion reporter").located(By.xpath("//span[@id='select2-chosen-10']"));
    public  static  final Target INFORMATION_SELECTED_REPORTER_MEETING_OPTION = Target.the("ingreso del reporter").located(By.xpath("//input[@id='s2id_autogen10_search']"));
    public  static  final Target BUTTON_INFORMATION_SELECTED_REPORTER_MEETING_OPTION = Target.the("boton ingreso del reporter").located(By.xpath("//div[@class='select2-result-label']"));
    public  static  final Target ATTENDEE_LIST_SELECT = Target.the("Click en opcion  Select contact to add").located(By.xpath("//span[@id='select2-chosen-12']"));
    public  static  final Target ATTENDEE_LIST_INPUT_INFORMATION= Target.the("Ingresar contacto").located(By.xpath("//input[@id='s2id_autogen12_search']"));
    public  static  final Target BUTTON_ATTENDEE_LIST_INPUT_INFORMATION = Target.the("boton ingreso del reporter").located(By.xpath("//div[@class='select2-result-label']"));
    public  static  final Target BUTTON_SAVE_MEETING_PROCESS = Target.the("Button save meeting").located(By.xpath("//div[@class='tool-button save-and-close-button icon-tool-button']"));
}
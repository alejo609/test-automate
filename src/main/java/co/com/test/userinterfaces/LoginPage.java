package co.com.test.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginPage {
    public static final Target EMAIL = Target.the("Input donde se ingresa el correo").located(By.id("StartSharp_Membership_LoginPanel0_Username"));
    public static final Target PASS = Target.the("Input donde se ingresa la contraseña").located(By.id("StartSharp_Membership_LoginPanel0_Password"));
    public static final Target BUTTON_LOGIN  = Target.the("Button Login").located(By.id("StartSharp_Membership_LoginPanel0_LoginButton"));

}

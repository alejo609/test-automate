package co.com.test.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CreateItemsPage {

    // creacion de meeting type
    public  static  final Target CREATE_ITEM_MEETING = Target.the("Item meeeting").located(By.xpath("(//a[@class='inplace-button inplace-create edit'])"));
    public static final Target INSERT_NAME_ITEM_MEETING = Target.the("Input donde se ingresa el nombre de meeting").located(By.xpath("//input[@name='Name']"));
    public static final Target BUTTON_INSERT_NEW_ITEM  = Target.the("boton para insertar la reunion").locatedBy("(//i[@class='fa fa-check-circle text-purple'])[2]");

    /// location
    public  static  final Target LOCATION_MEETING_BUTTON = Target.the("Location meeeting").located(By.xpath("(//a[@class='inplace-button inplace-create'] )[1]"));
    public  static  final Target TEXT_LOCATION_MEETING_BUTTON = Target.the("Text Location meeeting").located(By.xpath("//input[@name='Name']"));
    public  static  final Target BUTTON_LOCATION_MEETING = Target.the("Location meeeting").located(By.xpath("(//div[@class='tool-button save-and-close-button icon-tool-button'] )[2]"));



    // unit
    public  static  final Target UNIT_MEETING_BUTTON = Target.the("Unit Location meeeting").located(By.xpath("(//a[@class='inplace-button inplace-create'] )[2]"));

    public  static  final Target TEXT_SPACE_UNIT_MEETING_BUTTON = Target.the("Space unit Location meeeting").located(By.xpath("//input[@name='Name']"));
    public  static  final Target BUTTON_TEXT_SPACE_UNIT_MEETING_BUTTON = Target.the("Space unit Location meeeting").located(By.xpath("(//i[@class='fa fa-check-circle text-purple'])[2]"));


    // organized


    public  static  final Target ORGANIZEDBY_BUTTON = Target.the("OrganizedBY").located(By.xpath("(//a[@class='inplace-button inplace-create'] )[3]"));
    public  static  final Target ORGANIZEDBY_FIRSTNAME = Target.the("OrganizedBY firstname").located(By.xpath("//input[@name='FirstName']"));
    public  static  final Target ORGANIZEDBY_LASTNAME = Target.the("OrganizedBY lastname").located(By.xpath("//input[@name='LastName']"));
    public  static  final Target ORGANIZEDBY_EMAIL = Target.the("OrganizedBY Email").locatedBy("(//input[@class='editor s-Serenity-EmailAddressEditor s-EmailAddressEditor email required'] )");
    public  static  final Target BUTTON_ORGANIZEDBY = Target.the("OrganizedBY button").located(By.xpath("(//i[@class='fa fa-check-circle text-purple'])[2]"));


    // reporter



}

package co.com.test.questions;

import co.com.test.userinterfaces.LoginPage;
import co.com.test.utils.Constants;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.test.userinterfaces.Meeting.MEETING_NAME;

public class Meeting implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        Boolean comparate= MEETING_NAME.resolveFor(actor).getText().equals(Constants.ACTOR);
        System.out.println(comparate);
        return comparate;
    }

    public  static  Meeting meeting(){
        return new Meeting();
    }




}

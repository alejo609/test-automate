package co.com.test.stepdefinitions;

import co.com.test.questions.Meeting;
import co.com.test.tasks.*;
import co.com.test.utils.Constants;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import static co.com.test.utils.Constants.ACTOR;
import static co.com.test.utils.Constants.LINK_WEB;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class AccountStepDefinitions {
    @Before
    public void setUp(){
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled(Constants.ACTOR);
    }

    @Given("User has an active account and put their valid credentials")
    public void userHasAnActiveAccountAndPutTheirValidCredentials() {
        theActorCalled(ACTOR).wasAbleTo(Open.url(LINK_WEB));
        theActorCalled(ACTOR).attemptsTo(Login.login());
        theActorInTheSpotlight().attemptsTo(CreateUnit.createUnit());
        theActorInTheSpotlight().attemptsTo(CreateItems.createItems());
    }


    @When("User creates a meeting")
    public void userCreatesAMeeting() {
        theActorCalled(ACTOR).attemptsTo(MeetingProcess.meetingProcess());
    }

    @Then("User can see nee meeting in dashboard")
    public void userCanSeeNeeMeetingInDashboard() {
        theActorCalled(ACTOR).attemptsTo(SelectMeeting.selectMeeting());
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Meeting.meeting(), Matchers.is(true)));
    }
}